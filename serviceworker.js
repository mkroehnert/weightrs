// SPDX-License-Identifier: MIT
//
// WeightRS Authors: see AUTHORS.txt

// https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers
// https://github.com/mdn/sw-test

var CACHE = 'weightrs-v6'

var filesToCache = [
  '/weightrs-pages/',
  '/weightrs-pages/favicon.svg',
  '/weightrs-pages/index.html',
  //  '/weightrs-pages/serviceworker.js',
  //  '/weightrs-pages/weightrs.webmanifest',
  //  '/weightrs-pages/pkg/package.json',
  //  '/weightrs-pages/pkg/weightrs_bg.d.ts',
  //  '/weightrs-pages/pkg/weightrs.d.ts',
  '/weightrs-pages/pkg/weightrs_bg.wasm',
  '/weightrs-pages/pkg/weightrs.js',
  '/weightrs-pages/icons/icon-32.png',
  '/weightrs-pages/icons/icon-64.png',
  '/weightrs-pages/icons/icon-128.png'
]

this.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open(CACHE).then(function (cache) {
      return cache.addAll(filesToCache)
    })
  )
})

this.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(keyList.map(function (key) {
        if (key !== CACHE) {
          return caches.delete(key)
        }
      }))
    })
  )
  return self.clients.claim()
})

this.addEventListener('fetch', function (event) {
  // app shell files
  event.respondWith(
    caches.match(event.request).then(function (resp) {
      return resp || fetch(event.request)
    })
  )
})
