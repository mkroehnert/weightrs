// SPDX-License-Identifier: MIT
//
// WeightRS Authors: see AUTHORS.txt

use seed::prelude::*;

use web_sys::HtmlCanvasElement;

mod canvas;
mod db;
mod download;
mod model;
mod widgets;

#[wasm_bindgen(start)]
pub fn render() {
    seed::App::start("app", init, update, view);
}

fn init(_: Url, orders: &mut impl Orders<Message>) -> Model {
    let db = db::Db::new();

    orders.after_next_render(|_| Message::HtmlRendered());
    Model {
        app_state: AppState::Summary,
        db,
        measurement: model::WeightMeasurement {
            date: model::get_current_date(),
            time: "".into(),
            weight: "".into(),
            comment: "".into(),
        },
        form_valid: false,
        graph_canvas: ElRef::default(),
    }
}

fn update(msg: Message, model: &mut Model, orders: &mut impl Orders<Message>) {
    match msg {
        Message::HtmlRendered() => {
            if model.app_state == AppState::Summary {
                let data = model.db.get_latest_measurements(10);
                canvas::draw_graph_on(data, &model.graph_canvas);
                // prevent infinite loop
                orders.after_next_render(|_| Message::HtmlRendered()).skip();
            }
        }
        Message::NewInput() => {
            orders.send_msg(Message::DateInputChanged(model::get_current_date()));
            model.app_state = AppState::WeightInput;
        }
        Message::ShowTable() => {
            model.app_state = AppState::Table;
        }
        Message::DownloadImage() => {
            download::download_image_from_canvas(
                &format!("{}-weightrs-export.png", model::get_current_date()),
                &model.graph_canvas,
            );
        }
        Message::ClearDb() => {
            model.db.clear();
        }
        Message::DownloadDb() => {
            download::download_csv(
                &format!("{}-weightrs-export.csv", model::get_current_date()),
                &model.db.get_measurements(),
            );
        }
        Message::BackToSummary() => {
            switch_to_summary(model, orders);
        }
        Message::WeightFormSaved(time) => {
            model.measurement.time = time;
            model.db.upsert_measurement(&model.measurement);

            switch_to_summary(model, orders);
        }
        Message::WeightFormValid(form_valid) => {
            model.form_valid = form_valid;
        }
        Message::DateInputChanged(date) => {
            seed::log!("date", &date);
            model.measurement.date = date;
        }
        Message::TimeInputChanged(time) => {
            model.measurement.time = time;
        }
        Message::WeightInputChanged(weight) => {
            model.measurement.weight = weight;
        }
        Message::CommentInputChanged(comment) => {
            model.measurement.comment = comment;
        }
    }
}

fn view(model: &Model) -> impl IntoNodes<Message> {
    match model.app_state {
        AppState::Summary => widgets::summary_view(
            "summary",
            &model.graph_canvas,
            Message::NewInput,
            Message::ShowTable,
            Message::DownloadImage,
        ),
        AppState::Table => widgets::table_view(
            "table",
            &model.db.get_measurements(),
            Message::BackToSummary,
            Message::ClearDb,
            Message::DownloadDb,
        ),
        AppState::WeightInput => widgets::weight_input_form(
            "weight",
            &model.measurement,
            model.form_valid,
            Message::WeightFormSaved,
            Message::BackToSummary,
            Message::WeightFormValid,
            Message::DateInputChanged,
            Message::TimeInputChanged,
            Message::WeightInputChanged,
            Message::CommentInputChanged,
        ),
    }
}

fn switch_to_summary(model: &mut Model, orders: &mut impl Orders<Message>) {
    model.app_state = AppState::Summary;
    orders.after_next_render(|_| Message::HtmlRendered());
}

struct Model {
    app_state: AppState,
    db: db::Db,
    // input
    measurement: model::WeightMeasurement,
    form_valid: bool,
    graph_canvas: ElRef<HtmlCanvasElement>,
}

#[derive(Debug, Clone, PartialEq)]
enum AppState {
    Summary,
    Table,
    WeightInput,
}

impl Default for AppState {
    fn default() -> Self {
        AppState::Summary
    }
}

#[derive(Debug, Clone)]
enum Message {
    HtmlRendered(),
    BackToSummary(),
    // summary view
    NewInput(),
    ShowTable(),
    DownloadImage(),
    // table view
    ClearDb(),
    DownloadDb(),
    // input view
    WeightFormSaved(String),
    WeightFormValid(bool),
    DateInputChanged(String),
    TimeInputChanged(String),
    WeightInputChanged(String),
    CommentInputChanged(String),
}
