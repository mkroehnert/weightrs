// SPDX-License-Identifier: MIT
//
// WeightRS Authors: see AUTHORS.txt

use crate::model;
use seed::prelude::{LocalStorage, WebStorage};

pub struct Db {
    data: Vec<model::WeightMeasurement>,
}

const WEIGHT_DB_KEY: &str = "weights";

impl Db {
    pub fn new() -> Self {
        let data = LocalStorage::get(WEIGHT_DB_KEY).unwrap_or_else(|_| vec![]);

        Db { data }
    }

    pub fn get_measurements(&self) -> &Vec<model::WeightMeasurement> {
        &self.data
    }

    pub fn get_latest_measurements(&self, max_elements: usize) -> &[model::WeightMeasurement] {
        let index: usize = if self.data.len() < max_elements {
            0
        } else {
            self.data.len() - max_elements
        };
        &self.data[index..]
    }

    pub fn upsert_measurement(&mut self, measurement: &model::WeightMeasurement) {
        self.upsert_measurement_vec(measurement);
        self.store_data();
    }

    pub fn clear(&mut self) {
        self.data.clear();
        self.store_data();
    }

    fn store_data(&mut self) {
        LocalStorage::insert(WEIGHT_DB_KEY, &self.data).expect("saving to local storage");
    }

    fn upsert_measurement_vec(&mut self, measurement: &model::WeightMeasurement) {
        let pos = self
            .data
            .binary_search_by(|other| other.date.cmp(&measurement.date));
        match pos {
            Ok(pos) => self.data[pos] = measurement.clone(),
            Err(pos) => self.data.insert(pos, measurement.clone()),
        }
    }
}
