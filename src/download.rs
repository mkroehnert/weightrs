// SPDX-License-Identifier: MIT
//
// WeightRS Authors: see AUTHORS.txt

use seed::prelude::{js_sys, wasm_bindgen, web_sys, ElRef};

use wasm_bindgen::JsCast;

use web_sys::HtmlCanvasElement;

use crate::model;

pub fn download_csv(filename: &str, measurements: &Vec<model::WeightMeasurement>) {
    let mut data = String::from("Date;Time;Weight;Comment\n");
    measurements.iter().for_each(|el| {
        data.push_str(&format!(
            "{};{};{};\"{}\"\n",
            el.date, el.time, el.weight, el.comment
        ));
    });

    let encoded_data: String = js_sys::encode_uri_component(&data).into();
    let mime = String::from("data:text/plain;charset=utf-8");

    download_data_uri_as(&format!("{},{}", mime, encoded_data), filename);
}

pub fn download_image_from_canvas(filename: &str, canvas: &ElRef<HtmlCanvasElement>) {
    let canvas = canvas.get().expect("get canvas element");
    if let Ok(data_uri) = canvas.to_data_url_with_type("image/png") {
        download_data_uri_as(&data_uri, filename);
    }
}

fn download_data_uri_as(data_uri: &str, filename: &str) {
    let element = seed::document()
        .create_element("a")
        .expect("should be able to create element");

    let _ = element.set_attribute("href", data_uri);
    let _ = element.set_attribute("download", filename);

    let event = seed::document()
        .create_event("MouseEvents")
        .expect("should be able to call createEvent()")
        .dyn_into::<web_sys::MouseEvent>()
        .ok()
        .expect("should be a MouseEvent");
    event.init_mouse_event_with_can_bubble_arg_and_cancelable_arg("click", true, true);
    let _ = element.dispatch_event(&event);

    element.remove();
}
