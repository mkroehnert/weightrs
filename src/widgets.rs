// SPDX-License-Identifier: MIT
//
// WeightRS Authors: see AUTHORS.txt

use seed::{prelude::*, *};

use web_sys::HtmlCanvasElement;

use wasm_bindgen::JsCast;

use crate::model;

pub fn weight_input_form<M: 'static>(
    id: &str,
    measurement: &model::WeightMeasurement,
    form_valid: bool,
    on_save: impl FnOnce(String) -> M + 'static + Clone,
    on_cancel: impl FnOnce() -> M + 'static + Clone,
    on_valid_change: impl FnOnce(bool) -> M + 'static + Clone,
    on_date_change: impl FnOnce(String) -> M + 'static + Clone,
    _on_time_change: impl FnOnce(String) -> M + 'static + Clone,
    on_weight_change: impl FnOnce(String) -> M + 'static + Clone,
    on_comment_change: impl FnOnce(String) -> M + 'static + Clone,
) -> Node<M> {
    let input_style = style! {
        St::Padding => 0;
        St::Margin => 0;
        St::BoxSizing => "border-box";
        St::FontSize => "120%";
        St::Width => "100%";
        St::Border => "1px solid #CCC",
        St::BorderRadius => "0.25em",
    };
    let label_style = style! {
        St::Display => "inline-block";
        St::TextAlign => "left";
        St::Width => "100%";
    };

    let grid_style = style! {
        St::Display => "grid";
        St::GridTemplateColumns => "1fr 2fr";
        St::GridTemplateRows => "minmax(min-content auto) minmax(min-content auto)";
        St::RowGap => "1em";
        St::ColumnGap => "1em";
        St::AlignItems => "center";
    };

    form![
        style! {
            St::Margin => "0 auto";
            St::Padding => "1em";
            St::Border => "1px solid #CCC";
            St::BorderRadius => "1em";
        },
        attrs![
            At::Id => format!("{}-form", id),
            At::OnSubmit => "return false",
        ],
        // form events
        ev(Ev::Input, move |event| {
            event.prevent_default();

            let form_valid = event
                .current_target()
                .and_then(|t| t.dyn_into::<web_sys::HtmlFormElement>().ok())
                .and_then(|el| Some(el.check_validity()))
                .unwrap_or(false);

            on_valid_change(form_valid)
        }),
        ev(Ev::Submit, move |event| {
            event.prevent_default();
            let date = js_sys::Date::new_0();
            on_save(format!("{:02}:{:02}", date.get_hours(), date.get_minutes()))
        }),
        // form elements
        div![
            //header
            grid_style,
            label![
                style![
                    St::GridColumn => "1 / 3";
                    St::GridRow => "1";
                    St::TextAlign => "center";
                ],
                b!["Enter your Weight"],
            ],
            // weight input
            label![
                label_style.clone(),
                attrs![
                    At::For => format!("{}-form-input-number", id),
                ],
                "Weight (Kg)",
            ],
            input![
                input_style.clone(),
                attrs![At::Id => format!("{}-form-input-number", id),
                       At::Type => "text",
                       At::AutoFocus => true.as_at_value(),
                       At::Placeholder => 0.0,
                       At::Value => measurement.weight,
                       At::Required => true.as_at_value(),
                       At::Title => "Enter weight here (format: XY.Z with optional .Z)",
                       // TODO: switch decimal separator based on locale?
                       At::Pattern => r"\d\d*([,.]\d)?",
                ],
                ev(Ev::Input, |event| {
                    let element = event
                        .target()
                        .unwrap()
                        .unchecked_into::<web_sys::HtmlInputElement>();
                    on_weight_change(element.value().parse().unwrap_or_default())
                    //IF!(element.report_validity() => on_weight_change(element.value().parse().unwrap_or_default()))
                }),
            ],
            // date input
            label![
                label_style.clone(),
                attrs![
                    At::For => format!("{}-form-input-datetime", id),
                ],
                "Date",
            ],
            input![
                input_style.clone(),
                attrs![At::Id => format!("{}-form-input-datetime", id),
                       At::Type => "date", // datetime-local
                       At::Value => measurement.date,
                       At::Required => true.as_at_value(),
                ],
                // returned date string is guaranteed to be "yyyy-mm-dd"
                input_ev(Ev::Input, move |text| on_date_change(text)),
            ],
            /*
            // TODO: does not currently work, since it also expects input for seconds
            // time input
            label![
                    label_style.clone(),
                    attrs![
                        At::For => format!("{}-form-input-time", id),
                    ],
                    "Time",
                ],
            input![
                input_style.clone(),
                attrs![At::Id => format!("{}-form-input-time", id),
                       At::Type => "time",
                       At::Value => measurement.time,
                       At::Required => true.as_at_value(),
                       At::Step => 60,
                ],
                input_ev(Ev::Input, move |text| _on_time_change(text)),
            ],
            */
            // comment input
            label![
                label_style.clone(),
                attrs![
                    At::For => format!("{}-form-input-comment", id),
                ],
                "Comment",
            ],
            input![
                input_style.clone(),
                attrs![At::Id => format!("{}-form-input-comment", id),
                       At::Type => "text",
                       At::Value => measurement.comment,
                ],
                input_ev(Ev::Input, move |text| on_comment_change(text)),
            ],
            // cancel/submit form
            input![
                input_style.clone(),
                attrs![
                    At::Id => format!("{}-form-button-cancel", id),
                    At::Type => "button",
                    At::Value => "Cancel",
                ],
                mouse_ev(Ev::Click, move |_| on_cancel(),),
            ],
            input![
                input_style.clone(),
                attrs![
                    At::Id => format!("{}-form-button-save", id),
                    At::Type => "submit",
                    At::Value => "Save",
                    At::Disabled => (!form_valid).as_at_value(),
                ],
            ],
        ],
    ]
}

pub fn summary_view<M: 'static>(
    id: &str,
    canvas: &ElRef<HtmlCanvasElement>,
    on_new_input: impl FnOnce() -> M + 'static + Clone,
    on_show_table: impl FnOnce() -> M + 'static + Clone,
    on_download: impl FnOnce() -> M + 'static + Clone,
) -> Node<M> {
    grid_view(
        id,
        create_canvas(&canvas),
        "+",
        "Show Table",
        "Download Image",
        on_new_input,
        on_show_table,
        on_download,
    )
}

pub fn table_view<M: 'static>(
    id: &str,
    measurments: &Vec<model::WeightMeasurement>,
    on_back: impl FnOnce() -> M + 'static + Clone,
    on_clear: impl FnOnce() -> M + 'static + Clone,
    on_download: impl FnOnce() -> M + 'static + Clone,
) -> Node<M> {
    grid_view(
        id,
        render_table(measurments),
        "CLEAR",
        "Back",
        "Download CSV",
        on_clear,
        on_back,
        on_download,
    )
}

pub fn grid_view<M: 'static>(
    id: &str,
    placeholder: Node<M>,
    input1_str: &str,
    input2_str: &str,
    input3_str: &str,
    on_input1: impl FnOnce() -> M + 'static + Clone,
    on_input2: impl FnOnce() -> M + 'static + Clone,
    on_input3: impl FnOnce() -> M + 'static + Clone,
) -> Node<M> {
    let grid_style = style! {
        St::Display => "grid";
        St::Width => "100%";
        St::Height => "100%";
        St::GridTemplateColumns => "minmax(0, 1fr)";
        St::GridTemplateRows => "minmax(0, min-content) minmax(0, 1fr) minmax(0, min-content) minmax(0, min-content)";
        St::RowGap => "1em";
        St::ColumnGap => "1em";
        St::AlignItems => "center";
    };
    let input_style = style! {
        St::Padding => 0;
        St::Margin => 0;
        St::BoxSizing => "border-box";
        St::FontSize => "120%";
        St::Width => "100%";
        St::Border => "1px solid #CCC",
        St::BorderRadius => "0.25em",
    };

    div![
        grid_style,
        attrs![
            At::Id => format!("{}", id),
        ],
        input![
            input_style.clone(),
            attrs![
                At::Id => format!("{}-new-input", id),
                At::Type => "button",
                At::Value => input1_str,
            ],
            mouse_ev(Ev::Click, move |_| on_input1(),),
        ],
        div![
            style! {
                St::Width => "100%",
                St::Height => "100%",
                St::OverflowY => "auto",
            },
            placeholder,
        ],
        input![
            input_style.clone(),
            attrs![
                At::Id => format!("{}-clear-input", id),
                At::Type => "button",
                At::Value => input2_str,
            ],
            mouse_ev(Ev::Click, move |_| on_input2(),),
        ],
        input![
            input_style.clone(),
            attrs![
                At::Id => format!("{}-download", id),
                At::Type => "button",
                At::Value => input3_str,
            ],
            mouse_ev(Ev::Click, move |_| on_input3(),),
        ],
    ]
}

fn render_table<M: 'static>(measurements: &Vec<model::WeightMeasurement>) -> Node<M> {
    let table_head_style = style![
        St::Border => "5px solid rgb(190, 190, 190)",
        St::Padding => "5px 5px",
    ];

    table![
        style! {
            St::Width => "100%",
            St::MinHeight => "0",
            St::AlignSelf => "baseline",
            St::FontSize => "100%";
            St::Border => "2px solid #CCC";
            St::BorderRadius => "0.25em",
            St::BorderCollapse => "collapse",
        },
        attrs![
            At::Id => "measurements-table",
        ],
        colgroup![col![], col![],],
        thead![
            style! {},
            tr![
                th![table_head_style.clone(), "Date"],
                th![table_head_style.clone(), "Weight"],
            ],
        ],
        tbody![
            style![],
            measurements.into_iter().flat_map(|entry| vec![tr![
                attrs![
                    At::Title => entry.comment.clone(),
                ],
                td![
                    table_head_style.clone(),
                    format!("{} - {}", entry.date, entry.time)
                ],
                td![table_head_style.clone(), entry.weight.clone()],
            ]])
        ],
    ]
}

fn create_canvas<M: 'static>(canvas: &ElRef<HtmlCanvasElement>) -> Node<M> {
    canvas![
        el_ref(&canvas),
        style! {
            St::Width => "100%",
            St::Height => "100%",
            St::MinHeight => "0",
            St::FontSize => "100%";
        },
        attrs![
            At::Id => "measurements-graph",
        ],
    ]
}
