// SPDX-License-Identifier: MIT
//
// WeightRS Authors: see AUTHORS.txt

use seed::prelude::{ElRef, JsValue};

use web_sys::HtmlCanvasElement;

use crate::model;

pub fn draw_graph_on(measurments: &[model::WeightMeasurement], canvas: &ElRef<HtmlCanvasElement>) {
    let canvas = canvas.get().expect("get canvas element");

    // get canvas height / width ratio
    let rect = canvas.get_bounding_client_rect();

    let height = rect.height();
    let width = rect.width();

    canvas.set_width(width as u32);
    canvas.set_height(height as u32);

    let ctx = seed::canvas_context_2d(&canvas);

    // constants
    let x_text_width = 70.;
    let x_border = 10. + x_text_width;
    let y_text_height = 25.;
    let y_border = 10. + y_text_height;
    let stroke_width = 1.;
    // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Applying_styles_and_colors
    let line_offset = 0.5;
    let max_count = measurments.len();
    let padding = 50.;
    let x_step = (width as f64 - x_border - stroke_width - padding) / max_count as f64;

    // y
    let min_value = measurments
        .iter()
        .fold(1000., |min_val, el| el.weight_f64().min(min_val));
    let max_value = measurments
        .iter()
        .fold(0., |max_val, el| el.weight_f64().max(max_val));

    let y_smallest_value = min_value.floor();
    let y_biggest_value = max_value.ceil();
    let y_delta = y_biggest_value - y_smallest_value;
    let y_step = (height as f64 - y_border - stroke_width - 2. * padding) / y_delta;

    // draw background
    ctx.rect(0., 0., width as f64, height as f64);
    ctx.set_fill_style(&JsValue::from_str("#DFF"));
    ctx.fill();

    // draw coordinate system
    // y axis
    ctx.save();
    ctx.set_stroke_style(&JsValue::from_str("black"));
    ctx.set_line_width(stroke_width);
    ctx.begin_path();
    ctx.move_to(x_border + line_offset, 0.);
    ctx.line_to(x_border + line_offset, height - y_border);
    ctx.stroke();
    ctx.set_font("24px sans-serif");
    ctx.set_text_baseline("middle");
    for y_idx in 0..(y_delta as usize + 1) {
        let y_pos = line_offset + y_border + stroke_width + padding + y_idx as f64 * y_step;
        // number
        ctx.set_fill_style(&JsValue::from_str("black"));
        let _ = ctx.fill_text(
            &format!("{:>5.1}", y_biggest_value - y_idx as f64),
            1.,
            y_pos,
        );
        // short black line
        ctx.set_stroke_style(&JsValue::from_str("black"));
        ctx.begin_path();
        ctx.move_to(x_text_width, y_pos);
        ctx.line_to(x_border + line_offset, y_pos);
        ctx.stroke();
        // long grey line
        ctx.set_stroke_style(&JsValue::from_str("lightgrey"));
        ctx.begin_path();
        ctx.move_to(x_border, y_pos);
        ctx.line_to(width as f64, y_pos);
        ctx.stroke();
    }
    ctx.restore();
    // x axis
    ctx.save();
    ctx.set_stroke_style(&JsValue::from_str("black"));
    ctx.set_fill_style(&JsValue::from_str("black"));
    ctx.set_line_width(stroke_width);
    ctx.begin_path();
    ctx.move_to(x_border, height - y_border + line_offset);
    ctx.line_to(width, height - y_border + line_offset);
    ctx.set_font("20px sans-serif");
    ctx.set_text_baseline("ideographic baseline");
    for (x_idx, el) in measurments.iter().enumerate() {
        // small black line
        let x_pos = line_offset + x_border + stroke_width + padding + x_idx as f64 * x_step;
        ctx.move_to(x_pos, height - y_border + line_offset);
        ctx.line_to(x_pos, height - y_text_height);
        // date
        let x_text_middle_align = 26.;
        let _ = ctx.fill_text(
            &el.day_month_string(),
            x_pos - x_text_middle_align,
            height - 3.,
        );
    }
    ctx.stroke();
    ctx.restore();

    ctx.save();
    // shift coordinate system
    // to origin of the drawable graph area
    // enter weights as (date-index, - ( weight - lowest_y_floor_value))
    let x_translation = line_offset + x_border + stroke_width + padding;
    let y_translation = line_offset + y_border + stroke_width + padding + y_delta * y_step;
    let _ = ctx.translate(x_translation, y_translation);

    // divide into sections and calculate scale
    ctx.set_fill_style(&JsValue::from_str("#0BF"));
    ctx.set_stroke_style(&JsValue::from_str("#0BF"));
    let mut last_pos = (0., 0.);
    for (pos, el) in measurments.iter().enumerate() {
        let weight = el.weight_f64();
        let x_pos = pos as f64 * x_step;
        let y_pos = -(weight - y_smallest_value) * y_step;
        ctx.begin_path();
        let _ = ctx.arc(x_pos, y_pos, 3., 0., 3.14 * 2.);
        ctx.fill();
        if pos != 0 {
            ctx.bezier_curve_to(
                x_pos - x_step / 3.,
                y_pos,
                last_pos.0 + x_step / 3.,
                last_pos.1,
                last_pos.0,
                last_pos.1,
            );
            ctx.stroke();
        }
        last_pos = (x_pos, y_pos);
    }
    ctx.restore();
}
