// SPDX-License-Identifier: MIT
//
// WeightRS Authors: see AUTHORS.txt

use seed::prelude::js_sys;

use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct WeightMeasurement {
    pub date: String,
    pub time: String,
    pub weight: String,
    pub comment: String,
}

impl WeightMeasurement {
    pub fn weight_f64(&self) -> f64 {
        self.weight.parse().unwrap_or(0.)
    }

    /*pub fn year_str(&self) -> &str {
        &self.date[0..4]
    }*/

    pub fn month_str(&self) -> &str {
        &self.date[5..7]
    }

    pub fn day_str(&self) -> &str {
        &self.date[8..10]
    }

    pub fn day_month_string(&self) -> String {
        format! {"{}.{}.", self.day_str(), self.month_str()}
    }
}

pub fn get_current_date() -> String {
    let date = js_sys::Date::new_0();
    format!(
        "{}-{:02}-{:02}",
        date.get_full_year(),
        date.get_month() + 1,
        date.get_date()
    )
}
