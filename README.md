# WeightRS

WeighRS is a privacy friendly weight tracker.
It runs as a progressive web application in your browser, where also your data is stored (no servers involved except for serving the page).
Additionally, it is possible to download the displayed graph as PNG image and the weight data as CSV.

Visit https://mkroehnert.gitlab.io/weightrs-pages/ to run the application.

Also available via the distributed web:

* Hashbase: dat://weightrs.hashbase.io/
* Raw Dat : dat://57ff68366c24da6367d1f94118d1501db7604b6ba7f5ea8826b6e5a0f4f6f4c4/

Sourcecode (MIT) available here: https://mkroehnert.gitlab.io/weightrs

# Used Open Source Libraries #

* [seed (MIT)](https://seed-rs.org)
* [serde (MIT/Apache-2.0)](https://crates.io/crates/serde)
* [wasm-bindgen (MIT/Apache-2.0)](https://crates.io/crates/wasm-bindgen)
* [js-sys (MIT/Apache-2.0)](https://crates.io/crates/wasm-bindgen-futures)
+ [web-sys (MIT/Apache-2.0)](https://crates.io/crates/web-sys)

# License

The source code of this application is available under the MIT license.
See [LICENSE](LICENSE) for details.

# Authors

See [AUTHORS](AUTHORS.txt) for list of WeightRS authors and contributors
